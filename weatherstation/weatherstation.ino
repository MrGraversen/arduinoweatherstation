#include <Math.h>

int ldrPinIn = 0;
int thermistorIn = 5;
int ledSerial = 2;

void setup() 
{ 
  pinMode(ledSerial, OUTPUT);
  Serial.begin(9600);
}

void loop() 
{
  digitalWrite(ledSerial, HIGH);
  
  double light = readLdr();
  double temp = readThermistor();

  submit(light, temp);
  
  digitalWrite(ledSerial, LOW);
  
  delay(1000);
}

double readThermistor()
{
  return thermistor(analogRead(thermistorIn)) - 5; // minus 5 to correct a bit for cheap thermistor...
}

double readLdr()
{
  return analogRead(ldrPinIn);
}

double thermistor(double value) 
{
  double temp;
 
  temp = log(((10240000 / value) - 10000));
  temp = 1 / (0.001129148 + (0.000234125 * temp) + (0.0000000876741 * temp * temp * temp));
  temp = temp - 273.15;
  
  return temp;
}

void submit(double light, double temp)
{
  Serial.print(light);
  Serial.print(":");
  Serial.print(temp);
  Serial.print('\n');
}
